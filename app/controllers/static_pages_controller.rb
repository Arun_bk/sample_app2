class StaticPagesController < ApplicationController
  before_action :page
  def home
    if logged_in?
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end

  def help
  end

  def about
  end
  
  def contact
  end

  private
  def page
    if(params.has_key?(:page))
      page='static_pages/'+params[:page]
    else
      page='static_pages/home'
    end
  end
end
