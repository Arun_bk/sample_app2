class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
 before_action :set_locale
  private

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
    def set_locale
    
      # set locale to the locale on the request params or the default set in the config
   #   I18n.locale = params[:locale] || I18n.default_locale
     #  config.i18n.load_path += Dir[Rails_root.join('config', 'locales', '**', '*.{rb,yml}')]
    # I18n.load_path << Dir[ File.join(Rails.root, 'lib', 'locale', '*.{rb,yml}') ]
      # @local_dir instance variable to pass to the view for right-to-left and left-to-right languages
       I18n.locale = params[:locale] || session[:locale] || I18n.default_locale
  session[:locale] = I18n.locale


def default_url_options(options = {})
  { locale: I18n.locale }.merge options

   
      case I18n.locale
      when :fa, :ar
        @local_dir = "rtl"
      else
        @local_dir = "ltr"
      end
    end
   # I18n.default_locale = :fa
    # avoids having to add the locale to every link_to 
    def default_url_options
      { locale: I18n.locale }
    end
end
end